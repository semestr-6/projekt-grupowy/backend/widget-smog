package models

import "time"

//Installation is a list of sensors
type Installation struct {
	ID        int32    `json:"id"`
	Location  Location `json:"location"`
	Address   Address  `json:"address"`
	Elevation float64  `json:"-"`
	Airly     bool     `json:"-"`
	Sponsor   Sponsor  `json:"-"`
}

//Location is Latitude and Longitude of installation
type Location struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

//Address of installation
type Address struct {
	Country         string      `json:"country"`
	City            string      `json:"city"`
	Street          string      `json:"street"`
	Number          interface{} `json:"number"`
	DisplayAddress1 string      `json:"displayAddress1"`
	DisplayAddress2 string      `json:"displayAddress2"`
}

//Sponsor is person or organisation which pays for it
type Sponsor struct {
	ID          int         `json:"id"`
	Name        string      `json:"name"`
	Description string      `json:"description"`
	Logo        string      `json:"logo"`
	Link        interface{} `json:"link"`
}

/********************************************************/

type Measurements struct {
	Current  Current    `json:"current"`
	History  []History  `json:"-"`
	Forecast []Forecast `json:"-"`
}
type Values struct {
	Name  string  `json:"name"`
	Value float64 `json:"value"`
}
type Indexes struct {
	Name        string  `json:"name"`
	Value       float64 `json:"value"`
	Level       string  `json:"level"`
	Description string  `json:"description"`
	Advice      string  `json:"advice"`
	Color       string  `json:"color"`
}
type Standards struct {
	Name      string  `json:"name"`
	Pollutant string  `json:"pollutant"`
	Limit     float64 `json:"limit"`
	Percent   float64 `json:"percent"`
}
type Current struct {
	FromDateTime time.Time   `json:"fromDateTime"`
	TillDateTime time.Time   `json:"tillDateTime"`
	Values       []Values    `json:"values"`
	Indexes      []Indexes   `json:"indexes"`
	Standards    []Standards `json:"standards"`
}

//History , fuck History
type History struct {
	FromDateTime time.Time   `json:"fromDateTime"`
	TillDateTime time.Time   `json:"tillDateTime"`
	Values       []Values    `json:"values"`
	Indexes      []Indexes   `json:"indexes"`
	Standards    []Standards `json:"standards"`
}

//Forecast , fuck Forecast
type Forecast struct {
	FromDateTime time.Time     `json:"fromDateTime"`
	TillDateTime time.Time     `json:"tillDateTime"`
	Values       []interface{} `json:"values"`
	Indexes      []Indexes     `json:"indexes"`
	Standards    []interface{} `json:"standards"`
}
