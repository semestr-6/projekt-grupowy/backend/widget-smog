package airpollution

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	models "gitlab.com/semestr-6/projekt-grupowy/backend/widget-smog/airpollution/models"
)

var installation [5]models.Installation

func InitInstallation() {
	installation[0] = models.Installation{ID: 7, Location: models.Location{Latitude: 51.086225, Longitude: 17.012688998}, Address: models.Address{City: "Wrocław"}}
	installation[1] = models.Installation{ID: 13, Location: models.Location{Latitude: 51.75805, Longitude: 19.421231}, Address: models.Address{City: "Łódź"}}
	installation[2] = models.Installation{ID: 15, Location: models.Location{Latitude: 51.85669195, Longitude: 19.421231}, Address: models.Address{City: "Zgierz"}}
	installation[3] = models.Installation{ID: 17, Location: models.Location{Latitude: 50.057677996, Longitude: 19.926189}, Address: models.Address{City: "Kraków"}}
	installation[4] = models.Installation{ID: 26, Location: models.Location{Latitude: 52.219297995, Longitude: 21.004724}, Address: models.Address{City: "Warszawa"}}
}

func getAllInstallation(res http.ResponseWriter, req *http.Request) {

	json.NewEncoder(res).Encode(installation)
	return
}

func getInstallationListForTown(res http.ResponseWriter, req *http.Request) {

	keys, ok := req.URL.Query()["installationId"]

	if !ok || len(keys[0]) < 1 {
		res.WriteHeader(http.StatusNotFound)
		return
	}

	ID, err := strconv.ParseInt(keys[0], 10, 32)
	if err != nil {
		res.WriteHeader(http.StatusNotFound)
		return
	}

	installationID := int32(ID)
	var installationSelected models.Installation
	for _, item := range installation {
		if item.ID == installationID {
			installationSelected = item
		}
	}

	if installationSelected.ID == 0 {
		res.WriteHeader(http.StatusNotFound)
		return
	}
	r, _ := http.NewRequest("GET", "https://airapi.airly.eu/v2/installations/nearest", nil)
	r.Header.Add("Accept", "application/json")
	r.Header.Add("apikey", "8d10fa96196d45e5bc97d9c7fa9a5a36")

	q := r.URL.Query()
	q.Add("lat", strconv.FormatFloat(installationSelected.Location.Latitude, 'f', 6, 64))
	q.Add("lng", strconv.FormatFloat(installationSelected.Location.Longitude, 'f', 6, 64))
	q.Add("maxDistanceKM", "3")
	q.Add("maxResults", "20")

	r.URL.RawQuery = q.Encode()

	client := &http.Client{}

	resp, _ := client.Do(r)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		res.WriteHeader(http.StatusNotFound)
		return
	}

	var autoGenerated []models.Installation
	json.Unmarshal(body, &autoGenerated)

	json.NewEncoder(res).Encode(autoGenerated)
	return
}

func getInstallation(res http.ResponseWriter, req *http.Request) {

	keys, ok := req.URL.Query()["installationId"]

	if !ok || len(keys[0]) < 1 {
		res.WriteHeader(http.StatusNotFound)
		return
	}

	ID, err := strconv.ParseInt(keys[0], 10, 32)
	if err != nil {
		res.WriteHeader(http.StatusNotFound)
		return
	}

	installationID := int32(ID)

	r, _ := http.NewRequest("GET", "https://airapi.airly.eu/v2/measurements/installation", nil)
	r.Header.Add("Accept", "application/json")
	r.Header.Add("apikey", "8d10fa96196d45e5bc97d9c7fa9a5a36")

	q := r.URL.Query()
	q.Add("indexType", "AIRLY_CAQI")
	q.Add("installationId", fmt.Sprint(installationID))

	r.URL.RawQuery = q.Encode()

	client := &http.Client{}

	resp, _ := client.Do(r)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		res.WriteHeader(http.StatusNotFound)
		return
	}

	var measurements models.Measurements
	json.Unmarshal(body, &measurements)

	json.NewEncoder(res).Encode(measurements)
	return
}
