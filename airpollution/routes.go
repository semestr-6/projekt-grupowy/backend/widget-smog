package airpollution

import (
	"net/http"

	httplib "gitlab.com/semestr-6/projekt-grupowy/backend/go-libs/http-lib"
)

func GetRoutes() (routes httplib.Routes) {
	routes = httplib.Routes{
		httplib.Route{
			HttpMethod:  http.MethodGet,
			Route:       "/get-all-installations",
			HandlerFunc: getAllInstallation,
		},
		httplib.Route{
			HttpMethod:  http.MethodGet,
			Route:       "/get-installation-by-city",
			HandlerFunc: getInstallationListForTown,
		},

		httplib.Route{
			HttpMethod:  http.MethodGet,
			Route:       "/get-installation",
			HandlerFunc: getInstallation,
		},
	}
	return
}
