package main

import (
	_ "github.com/lib/pq"
	httplib "gitlab.com/semestr-6/projekt-grupowy/backend/go-libs/http-lib"
	"gitlab.com/semestr-6/projekt-grupowy/backend/widget-smog/airpollution"
)

func main() {
	routes := getRoutes()
	airpollution.InitInstallation()
	httplib.DeclareNewRouterAndStart(routes)
}

func getRoutes() (routes httplib.Routes) {
	routes = append(routes, airpollution.GetRoutes()...)
	return
}
